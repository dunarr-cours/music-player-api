<?php

namespace App\Command;

use App\Entity\Folder;
use App\Entity\Track;
use App\Repository\FolderRepository;
use App\Repository\TrackRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;

#[AsCommand(name: 'app:scan-folder', description: 'Add a short description for your command',)]
class ScanFolderCommand extends Command
{
    private FolderRepository $folderRepository;
    private TrackRepository $trackRepository;

    /**
     * @param FolderRepository $folderRepository
     * @param TrackRepository $trackRepository
     */
    public function __construct(FolderRepository $folderRepository, TrackRepository $trackRepository)
    {
        $this->folderRepository = $folderRepository;
        $this->trackRepository = $trackRepository;
        parent::__construct();
    }


    protected function configure(): void
    {
        $this->addArgument('folder', InputArgument::OPTIONAL, 'Folder to scan');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $folder = $input->getArgument('folder');

        if (!$folder) {
            $io->error("you must pass a folder");
        }

        $foldersEntities = $this->folderRepository->findAll();
        $folders = [];
        foreach ($foldersEntities as $folder) {
            $folders[$folder->getPath()] = $folder;
        }


        $finder = new Finder();
        $finder->files()->in($folder)->name("*.flac");

        foreach ($finder as $file) {
            if(!isset($folders[$file->getRelativePath()])){
                $folder = new Folder();
                $folder->setPath($file->getRelativePath());
                $folders[$file->getRelativePath()] = $folder;
                $this->folderRepository->add($folder);
            }
            $track =  new Track();
            $track->setPath($file->getPathname());
            $track->setName($file->getFilenameWithoutExtension());
            $track->setFolder($folders[$file->getRelativePath()]);
            $this->trackRepository->add($track);
            $io->info($track->getName());
        }

        return Command::SUCCESS;
    }
}
