<?php

namespace App\Controller;

use App\Entity\Folder;
use App\Entity\Track;
use App\Repository\FolderRepository;
use App\Repository\TrackRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Stream;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class MainController extends AbstractController
{
    #[Route('/track/{track}', methods: ["GET"])]
    public function readTrack(Track $track): BinaryFileResponse
    {
        $response = new Stream($track->getPath());
        return $this->file($response);
    }

    #[Route('/folders', methods: ["GET"])]
    public function folderIndex(FolderRepository $folderRepository): JsonResponse
    {
        return $this->json($folderRepository->findAll(), context: ['groups' => "folder_list"]);
    }

    #[Route('/folders/{folder}', methods: ["GET"])]
    public function folder(Folder $folder): JsonResponse
    {
        return $this->json($folder, context: ['groups' => "folder_details"]);
    }

    #[Route('/search', methods: ["GET"])]
    public function search(Request $request, TrackRepository $trackRepository, FolderRepository $folderRepository, NormalizerInterface $normalizer): JsonResponse
    {
        $tracks = $trackRepository->search($request->get("q"));
        $folders = $folderRepository->search($request->get("q"));
        $folders = $normalizer->normalize($folders, context: ['groups' => "folder_list"]);
        $tracks = $normalizer->normalize($tracks, context: ['groups' => "folder_details"]);
        return $this->json(compact("tracks", "folders"), context: ['groups' => "folder_details"]);
    }
}
